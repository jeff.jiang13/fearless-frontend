function createCard(name, description, pictureUrl,iStartDate,iEndDate, location) {
    return `
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          <small class="text-muted">${iStartDate} ${"-"} ${iEndDate}</small>
              </div>

        </div>
      </div>
    `;
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        let i = 0
    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl)

        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const description = details.conference.description
            const title = details.conference.name;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;


            let startDate = details.conference.starts;
            let d1 = new Date(startDate)
            let iStartDate = (d1.getMonth()+1) + "/" + d1.getDate() + "/" + d1.getFullYear()
            let endDate = details.conference.ends;
            let d2 = new Date(endDate);
            let iEndDate = (d2.getMonth()+1) + "/" + d2.getDate() + "/" + d2.getFullYear()
            const html = createCard(title, description, pictureUrl, iStartDate,iEndDate, location);
            const column = document.querySelectorAll('.col');
            column[i % 3].innerHTML += html;
            i++


        }
    }
    }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
      const myerror = error()
      const errorlocation = document.querySelector('body')
      errorlocation.innerHTML += myerror

    }

});
